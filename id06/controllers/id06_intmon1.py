from __future__ import annotations

import numpy
import enum
from bliss.controllers.bliss_controller import BlissController
from bliss.common.utils import BOLD
from bliss.shell.standard import umv


class _Gains(enum.Enum):
    HIGH = [1, 0, 0, 0]
    MEDIUM = [0, 1, 0, 0]
    LOW = [0, 0, 1, 0]


def angular_deg_dist(a, b):
    """Returns the smallest angular oriented distance between `a` and `b`

    The result `r` follow this equality `(a + r) % 2π == b % 2π   -π <= r <= π'

    Arguments:
        a: An angle in degree
        b: An angle in degree
    """
    delta = numpy.deg2rad(b) - numpy.deg2rad(a)
    return numpy.rad2deg(numpy.arctan2(numpy.sin(delta), numpy.cos(delta)))


class ID06IntMon1(BlissController):
    """
    Intensity monitor mounted of the "wheel1".
    """
    def __init__(self, config):
        super().__init__(config)

    @property
    def counters(self):
        return self._taco.counters + self._calc.outputs

    def _load_config(self):
        self._taco = self.config["taco"]
        self._wago = self.config["wago"]
        self._calc = self.config["calc"]
        self._foil_wheel = self.config["foil_wheel"]
        self._foil_tolerence = self.config["foil_tolerence"]
        self._foils = self.config["foils"]

    def _data_to_gain(self, data):
        if data == _Gains.LOW.value:
            return "LOW"
        if data == _Gains.MEDIUM.value:
            return "MEDIUM"
        if data == _Gains.HIGH.value:
            return "HIGH"
        return str(data)

    def _read_gains(self):
        do = self._wago.get("do")
        d_ul = do[8:12]
        d_dl = do[12:16]
        d_dr = do[16:20]
        d_ur = do[20:24]
        ul = self._data_to_gain(d_ul)
        dl = self._data_to_gain(d_dl)
        ur = self._data_to_gain(d_ur)
        dr = self._data_to_gain(d_dr)
        return ul, dl, ur, dr

    @property
    def gain(self) -> self:
        """Get/set gain of Novelec amplifiers of the intensity monitor

        Arguments:
            gain: One of: 1, "low", 2, "medium", 3, "high"
        """
        ul, dl, ur, dr = self._read_gains()
        if ul != dl or dr != dr or ur != dr:
            raise ValueError(f"Diodes dont have the same gain: ul:{ul}, dl:{dl}, ur:{ur}, dr:{dr}")

        return ul

    @gain.setter
    def gain(self, gain: int | str):
        if isinstance(gain, str):
            gain = gain.upper()

        if gain == 3 or gain == "HIGH":
            gain = _Gains.HIGH
        elif gain == 2 or gain == "MEDIUM":
            gain = _Gains.MEDIUM
        elif gain == 1 or gain == "LOW":
            gain = _Gains.LOW
        else:
            raise ValueError(f"Gain {gain} is not supported. One of 1, 2, 3, 'LOW', 'MEDIUM', 'HIGH' are supported")

        do = self._wago.get("do")
        do[8:12] = gain.value
        do[12:16] = gain.value
        do[16:20] = gain.value
        do[20:24] = gain.value
        self._wago.set("do", do)
        print(f"Intensity Monitor gain set to {gain.name}.")

    @property
    def foil(self) -> str:
        current_pos = self._foil_wheel.position
        for desc in self._foils:
            selected = abs(angular_deg_dist(current_pos, desc["pos"])) < self._foil_tolerence
            if selected:
                return desc["name"]
        return None

    def _get_foi__from_name(self, foil: str):
        foil = foil.upper()
        for desc in self._foils:
            if foil == desc["name"].upper():
                return desc
        raise ValueError(f"'{foil}' is not a valid foil name")

    @foil.setter
    def foil(self, foil):
        desc = self._get_foi__from_name(foil)
        current_pos = self._foil_wheel.position
        move_dist = angular_deg_dist(current_pos, desc["pos"])
        if abs(move_dist) < self._foil_tolerence:
            print(f"No action taken, {self._foil_wheel.name} already at the '{desc['name']}' position")
            return

        new_position = current_pos + move_dist
        if new_position > 720:
            new_position -= 360
        if new_position < -720:
            new_position += 360

        umv(self._foil_wheel, new_position)

    def __info__(self):
        ul, dl, ur, dr = self._read_gains()
        result = ""
        result += f"Intensity monitor {self.name}\n"
        result += "\n"
        try:
            gain = self.gain
            result += f"GAIN: {gain:<6}\n"
            result += "       Available: 1, LOW; 2, MEDIUM; 3, HIGH\n"
            result += "\n"
        except ValueError:
            pass
        result += f"GAINS (per diodes):  ul {ul:<6}  ur {ul:<6}\n"
        result += f"                     dl {dl:<6}  dr {dr:<6}\n"

        foil = self.foil
        result += f"FOIL: {foil:<6}\n"
        result += f"      {self._foil_wheel.position:6.2f}\n"
        result += "\n"
        result += f"FOILS:\n"
        for desc in self._foils:
            selected = foil == desc["name"]
            indicator = "-->" if selected else "   "
            name = desc["name"]
            description = desc["description"]
            if selected:
                line = BOLD(f"{indicator} {name:6s}  {description:s}\n")
            else:
                line = f"{indicator} {BOLD(f'{name:6s}')}  {description:s}\n"
            result += f"  {line}"
        return result


"""
def _intmon1_state():
    config = get_config()
    wheel1 = config.get("wheel1")



def intmon1(foil=None):
    ""
    Move a different filter of the "wheel1" intensity monitor into the beam.

    Whithout argument, it shows the actual filter placed in the beam.

    To see all possible foils, type `intmon1()`
    ""
    if foil is None or foil == "?" or foil == "help":
        _intmon1_state()
        return

    _intmon1_move(foil)



"""
