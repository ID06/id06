
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
import tabulate
import gevent
import time
from scipy import signal
import glob


from bliss import setup_globals
from bliss.config import settings
from bliss.config.static import get_config
from bliss.shell.standard import umv
from bliss.scanning.chain import ChainPreset
from bliss.common.utils import ColorTags, BOLD, RED, ORANGE, BLUE, GREEN
from bliss.common.plot import plot
from bliss.controllers.bliss_controller import BlissController
from bliss.controllers.monochromator.monochromator import MonochromatorFixExit
from bliss.controllers.monochromator.tracker import EnergyTrackingObject


class ID06CinelMonochromator(MonochromatorFixExit):
    
    def __init__(self, config):
        super().__init__(config)
                
    """
    Load Configuration
    """
    def _load_config(self):
        super()._load_config()
        
        """
        Motors
        """
        self._motors["bragg_fix_exit"] = None
        motors_conf = self.config.get("motors", None)        
        for motor_conf in motors_conf:            
            # Bragg Fe
            if "bragg_fix_exit" in motor_conf.keys():
                self._motors["bragg_fix_exit"] = motor_conf.get("bragg_fix_exit")
                self._motors["bragg_fix_exit"].controller._set_mono(self)
        
        """
        Distance between crystals is manage by a motor (xtal) moving 
        second crystals perpendicular to the crystal plane.
        dxtals_at_xtal0 is the distance between first and second crystal when
        the motor (xtal) is at zero position
        Positive motion is intended to get both crystals closer.
        It will be taken into account for all xtals if "dxtal_at_xtal0"
        parameter is present in the monochromator yml file or
        xtal by xtal if present in the xtals yml object
        """
        dxtal_at_xtal0 = self.config.get("dxtal_at_xtal0", None)
        if dxtal_at_xtal0 is not None:
            self._dxtal_at_xtal0 = {}
            self._dxtal_at_xtal0["all"] = float(dxtal_at_xtal0)
        xtal_xtal0 = self._xtals.get_xtals_config("dxtal_at_xtal0")
        if xtal_xtal0 is not None:
            if hasattr(self, "_dxtal_at_xtal0"):
                self._dxtal_at_xtal0.update(self._xtals.get_xtals_config("dxtal_at_xtal0"))        
            else:
                self._dxtal_at_xtal0 = xtal_xtal0
        
        """
        get trajectory object
        """
        self.trajectory = self.config.get("trajectory", None)
        if self.trajectory is not None:
            self.trajectory._set_mono(self)
        
        """
        Needed to check that trajectory is already loaded
        """
        self._traj_config = {
            "mode": None,
            "start": None,
            "stop": None,
            "points": None,
            "time": None,
        }

            
    def _init(self):
        """
        Initialization
        """
        super()._init()
        
        # Trajectory
        
    def __info__(self):
        print(f"\n{self._info_mono()}\n")
        print(f"{self._info_xtals()}\n")
        print(f"{self._info_motor_energy()}\n")
        print(f"{self._info_motor_fix_exit()}\n")
        print(f"{self._info_motor_tracking()}\n")
        return ""
        
    def _info_motor_fix_exit(self):
        if self._motors["bragg_fix_exit"] is not None:
            controller = self._motors["bragg_fix_exit"].controller
            # TITLE
            title = ["", ""]
            title.append(BLUE(self._motors["bragg_fix_exit"].name))
            for axis in controller.reals:
                title.append(BLUE(axis.name))
            # CALCULATED POSITION ROW
            calculated = ["", "Calculated"]
            # bragg_fe
            # bragg
            rbragg = controller._tagged["bragg"][0]
            calculated.append(f"{rbragg.position:.3f} {rbragg.unit}")
            calculated.append(f"{rbragg.position:.3f} {rbragg.unit}")
            # xtal
            calc_xtal = self.bragg2xtal(rbragg.position)
            for axis in controller.reals:
                if controller._axis_tag(axis) != "bragg":
                    calculated.append(f"{calc_xtal:.3f} {axis.unit}")
            #
            # CURRENT POSITION ROW
            #
            current = ["", "Current"]
            # bragg fix exit
            current.append(f"{controller.pseudos[0].position:.3f} {controller.pseudos[0].unit}")
            for axis in controller.reals:
                current.append(f"{axis.position:.3f} {axis.unit}")
                
            info_str = tabulate.tabulate(
                [calculated, current],
                headers=title,
                tablefmt="plain",
                stralign="right",
            )
            return f"{info_str}"
        else:
            return ""

    """
    Distance between xtals when xtal motor is at 0 position
    """
    def _get_dxtal_at_xtal0(self):
        if hasattr(self, "_dxtal_at_xtal0"):
            if self._xtals.xtal_sel in self._dxtal_at_xtal0.keys():
                return self._dxtal_at_xtal0[self._xtals.xtal_sel]
            if "all" in self._dxtal_at_xtal0.keys():
                return self._dxtal_at_xtal0["all"]
        else:
            raise ValueError(f"Monochromator {self._name}: No dxtal_at_xtal0 defined")
            
    @property
    def dxtal_at_xtal0(self):
        return self._get_dxtal_at_xtal0()
        
    """
    Energy related methods, specific to Fix Exit Mono
    """        
    @property
    def fix_exit_offset(self):
        return self._fix_exit_offset

    @fix_exit_offset.setter
    def fix_exit_offset(self, value):
        self._fix_exit_offset = value

    def dxtal2xtal(self, dxtal):
        return self.dxtal_at_xtal0 - dxtal

    def xtal2dxtal(self, xtal):
        return self.dxtal_at_xtal0 - xtal

    def bragg2xtal(self, bragg):
        dxtal = self.bragg2dxtal(bragg)
        xtal = self.dxtal2xtal(dxtal)
        return xtal

    def xtal2bragg(self, xtal):
        dxtal = self.xtal2dxtal(xtal)
        bragg = self.dxtal2bragg(dxtal)
        return bragg

    def energy2xtal(self, ene):
        bragg = self.energy2bragg(ene)
        dxtal = self.bragg2dxtal(bragg)
        xtal = self.dxtal2xtal(dxtal)
        return xtal

    #
    # TRAJECTORY

    def _load_raw_trajectory(self, Estart, Estop, traj_mode):
        """
        Generic method: TO BE RE-WRITTEN
        """
        self._traj_dict = {}
        
        # Number of points in the trajectory
        start_th = self.energy2bragg(Estart) - self._bragg_offset.get()
        end_th = self.energy2bragg(Estop) - self._bragg_offset.get()
        from_fjs = self.bragg2xtal(start_th)
        to_fjs   = self.bragg2xtal(end_th)
        nbp      = int( abs(from_fjs - to_fjs) / self._traj_resolution ) + 1
        traj_nbp_max = self._motors["trajectory"]._get_max_traj_point()
        if nbp > traj_nbp_max:
            nbp = traj_nbp_max
            new_res = abs(from_fjs - to_fjs) / (nbp - 1)
            print(RED(f"DECIMATION => 1 point every {new_res:.6f}mm for FJS "))
        
        # Build energy data array
        fjs_data = numpy.linspace(from_fjs, to_fjs, nbp)
        bragg_data = self.xtal2bragg(fjs_data)
        self._ene_data = self.bragg2energy(bragg_data + self._bragg_offset.get())

        # Calculate trajectory positions
        bragg_pos = self._motors["real_bragg"].position
        virtual_ene_pos = self.bragg2energy(bragg_pos)
        if traj_mode == "energy":
            self._traj_data = self._ene_data
        elif traj_mode == "undulator":
            self._traj_data = self.undulator_master.tracking.energy2tracker(self._ene_data)
        elif traj_mode == "bragg":
            self._ene_data = numpy.flip(self._ene_data)
            self._traj_data = self.energy2bragg(self._ene_data) - self._bragg_offset.get()
        else:
            raise ValueError("Unknown Trajectory Mode ")

        # get monochromator trajectory
        traj_mono_dict = self._get_monochromator_trajectory(self._ene_data)

        # get trackers trajectories
        traj_tracker_dict = self._get_tracker_trajectory(self._ene_data)

        # get virtual axis trajectory
        traj_virtual_dict = {
            self._motors["virtual_energy"].name: numpy.copy(self._ene_data)
        }

        # set the virtual energy position to the current virtual energy
        self._motors["virtual_energy"].dial = virtual_ene_pos
        self._motors["virtual_energy"].offset = 0

        # load trajectory in TrajectoryMotor
        self._traj_dict.update(traj_mono_dict)
        self._traj_dict.update(traj_tracker_dict)
        self._traj_dict.update(traj_virtual_dict)
        self._motors["trajectory"].set_positions(
            self._traj_data,
            self._traj_dict,
            self._motors["trajectory"].LINEAR
        )

    def _load_trajectory(self, from_ene, to_ene, traj_mode, nb_points=None, time_per_point=None):
        """
        Generic method
        """
        
        if not self._is_trajectory_loaded(from_ene, to_ene, traj_mode, nb_points, time_per_point):
            self._load_raw_trajectory(from_ene, to_ene, traj_mode)

            # Load a trajectory allowing continuous scan undershoot
            if nb_points is not None and time_per_point is not None:
                self.master_motor = self._get_monochromator_acquisition_master_motor(
                    from_ene,
                    to_ene,
                    nb_points,
                    time_per_point,
                    traj_mode,
                )

                Estart_real = from_ene - 2 * self.master_motor.undershoot
                Estop_real = to_ene + 2 * self.master_motor.undershoot
                #if traj_mode == "energy":
                    #Estart_real = from_ene - 2 * self.master_motor.undershoot
                    #Estop_real = to_ene + 2 * self.master_motor.undershoot
                #elif traj_mode == "bragg":
                    #bragg_start_real = self.energy2bragg(from_ene) + 2 * self.master_motor.undershoot
                    #bragg_stop_real = self.energy2bragg(to_ene) - 2 * self.master_motor.undershoot
                    #Estart_real = self.bragg2energy(bragg_start_real)
                    #Estop_real = self.bragg2energy(bragg_stop_real)
                #elif traj_mode == "undulator":
                    
                    #undu_master_name = self.undulator_master.name
                    
                    #undu_real_start = self.master_motor.movables_params[undu_master_name]["start"]
                    #E_real_start = abs(from_ene - self.undulator_master.tracking.tracker2energy(undu_real_start))
                    #E_undershoot_start = abs(from_ene - E_real_start)
                    #Estart_real = from_ene - 2 * E_undershoot_start
                    
                    #undu_real_stop = self.master_motor.movables_params[undu_master_name]["stop"]
                    #E_real_stop = abs(to_ene - self.undulator_master.tracking.tracker2energy(undu_real_stop))
                    #E_undershoot_stop = abs(to_ene - E_real_stop)
                    #Estop_real = to_ene + 2 * E_undershoot_stop
                    
                #else:
                    #RuntimeError(f"Trajectory mode \"{traj_mode}\" Unknown")

                self._load_raw_trajectory(Estart_real, Estop_real, traj_mode)
                
                # Save loaded trajectory
                self._traj_config["mode"] = traj_mode
                self._traj_config["start"] = from_ene
                self._traj_config["stop"] = to_ene
                self._traj_config["points"] = nb_points
                self._traj_config["time"] = time_per_point
        else:
            print(GREEN("Trajectory already loaded"))
            
    def _is_trajectory_loaded(self, from_ene, to_ene, traj_mode, nb_points, time_per_point):
        
        if self._traj_config["mode"] == traj_mode and \
           self._traj_config["start"] == from_ene and \
            self._traj_config["stop"] == to_ene and \
            self._traj_config["points"] == nb_points and \
            self._traj_config["time"] == time_per_point:
            return True
        else:
            return False
           

    def _get_monochromator_trajectory(self, ene_data):
        
        data = numpy.copy(ene_data)
        
        bragg_data = self.energy2bragg(data) - self._bragg_offset.get()
        mono_dict = {
            "mbrag": numpy.copy(bragg_data),
            "msafe": numpy.copy(bragg_data),
            "mcoil": numpy.copy(bragg_data),
        }
        
        fjsrx = numpy.copy(bragg_data)
        fjsry = numpy.copy(bragg_data)
        (offset_rx, offset_ry) = self.calib.fjs.offset
        
        fjsz = self.bragg2xtal(bragg_data)
        fjsrx[:] = offset_rx
        fjsry[:] = offset_ry
        (fjsur, fjsuh, fjsd) = self.xyz2fjs(fjsrx, fjsry, fjsz)
        mono_dict["mfjsur"] = fjsur
        mono_dict["mfjsuh"] = fjsuh
        mono_dict["mfjsd"] = fjsd

        return mono_dict

    def plot(self, x, y):
        import matplotlib.pyplot as plt
        import matplotlib
        fig = plt.figure()
        plt.plot(x, y)
        plt.show()

    def get_energy_from_undulator(self, undu_val):
        diff_arr = numpy.absolute(self._traj_data-undu_val)
        ind = diff_arr.argmin()
        return (self._traj_dict["mdcm_virtual_energy"][ind],ind)
