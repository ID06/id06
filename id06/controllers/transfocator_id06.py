import time
from random import randint
from gevent import Timeout, sleep
import tabulate
from bliss import global_map
from bliss.common.utils import grouped
from bliss.controllers.wago.wago import WagoController, ModulesConfig, get_wago_comm
from bliss.config import channels
from bliss.common.event import dispatcher
from bliss.common.protocols import HasMetadataForScan
from bliss.common.logtools import log_debug, log_debug_data, log_error, log_exception
from typing import List, Optional
from bliss.common.utils import autocomplete_property
from bliss.common.utils import BOLD


TFLSON = False
TFLSOFF = True


def _display(status):
    if status == "MOV":
        return "---"
    return status


def _encode(status):
    if status is None:
        return None
    if status in (1, "in", "IN", True):
        return "IN"
    if status in (0, "out", "OUT", False):
        return "OUT"
    raise ValueError("Invalid position {!r}".format(status))


class TransfocatorID06(HasMetadataForScan):
    """
    Manages the transfocator device through WAGO DAC and I/O.

    It was designed to expose the same API of the BLISS class `Transfocator`
    to be able to use tflens. Unfortunatly tflens was not really mapping.
    So was also redesigned.

    The lenses are controlled pneumatically via WAGO output modules.

    The position is read from WAGO input modules.

    It uses 3 channels:
    - One for the motion, containing voltage
    - One for the switches In,OUT for each lences
    - One for enabling the motions
    """

    def __init__(self, name, config):
        self.name = name
        self.exec_timeout = int(config.get("timeout", 3))
        self.safety = bool(config.get("safety", False))
        self.display_during_move = True

        self._vin = float(config["vin"])
        assert self._vin > 0
        self._vout = float(config["vout"])
        assert self._vout < 0
        self._vstop = float(config["vstop"])
        self._dac_channel = str(config["dac_channel"])
        self._switch_channel = str(config["switch_channel"])
        self._enable_channel = str(config["enable_channel"])

        # first attempt to instantiate wago connection
        # with controller_ip and controller_port
        try:
            self.wago_ip = config["controller_ip"]
        except KeyError:
            # if not provided attempt to get wago reference
            self.wago = config["wago"]
        else:
            self.wago_port = config.get("controller_port", 502)
            self.wago = None
        self.empty_jacks = []
        self.pinhole = []
        self.simulate = config.get("simulate", False)
        self._state_chan = channels.Channel(
            "transfocator: %s" % name, callback=self.__state_changed
        )
        if self.wago:
            global_map.register(self, children_list=[self.wago])
        else:
            global_map.register(self)

        if "lenses" in config:
            self.nb_lens = int(config["lenses"])
            self.nb_pinhole = int(config.get("pinhole", 0))

            if self.nb_pinhole == 2:
                # pinholes are always the first and the last channels
                self.pinhole = [0, self.nb_lens + 1]
            elif self.nb_pinhole == 1:
                # the pinhole is always the first channel
                self.pinhole = [0]
        else:
            layout = config["layout"].strip()
            lenses = []
            for _i, _c in enumerate(layout):
                if _c == "X":
                    self.empty_jacks.append(_i)
                elif _c == "P":
                    self.pinhole.append(_i)
                elif _c == "L":
                    lenses.append(_i)
                else:
                    raise ValueError(f"{name}: layout: unknown element `{_c}'")

            self.nb_lens = len(lenses) + len(self.empty_jacks)
            self.nb_pinhole = len(self.pinhole)
            if self.nb_pinhole > 2:
                raise ValueError(f"{name}: layout can only have 2 pinholes maximum")

        self._disabled = [(i in self.empty_jacks) for i in range(20)]
        self._nb_actuators = self.nb_pinhole + self.nb_lens
        super().__init__()

    def scan_metadata(self):
        metadata = self._status_dict()
        metadata["@NX_class"] = "NXcollection"
        return metadata

    def connect(self):
        """Connect to the WAGO module, if not already done"""
        if self.wago is None:
            mapping = TfWagoMapping(self.nb_lens, self.nb_pinhole)

            modules_config = ModulesConfig(str(mapping), ignore_missing=True)

            conf = {"modbustcp": {"url": f"{self.wago_ip}:{self.wago_port}"}}

            comm = get_wago_comm(conf)
            self.wago = WagoController(comm, modules_config)
            global_map.register(self, children_list=[self.wago])

    def close(self):
        """Close the connection with the wago"""
        if self.wago:
            self.wago.close()

    def __close__(self):
        self.close()

    def _read_positions(self) -> List[str]:
        """Returns the positions of the switches.

        - If "IN" the switch in the beam is on
        - If "OUT" the switch out of the beam is on
        - If "MOV" both switches are on or off (intermediate state)
        """
        switches = self.wago.get(self._switch_channel)

        def resolve_switch(in_switch, out_switch):
            if in_switch == TFLSON and out_switch == TFLSOFF:
                return "IN"
            if in_switch == TFLSOFF and out_switch == TFLSON:
                return "OUT"
            return "MOV"
        result = []
        for i in range(0, len(switches), 2):
            result.append(resolve_switch(switches[i], switches[i + 1]))
        return result

    def _move_positions(self, positions: List[Optional[bool]], force=False):
        """
        Move all the actuators according to this positions.

        - If the position is "IN" then the corresponding actuator is moved in the beam.
        - If the position is "OUT" then the actuator is moved out.
        - If the position is None then the actuator is skipped.
        """
        vin = self._vin
        vout = self._vout
        vstop = self._vstop
        positions = [None if e else p for p, e in zip(positions, self._disabled)]

        def compute_motion(actually_in, expected_in):
            if expected_in is None:
                return vstop
            if expected_in == actually_in:
                return vstop
            if expected_in == "IN":
                return vin
            if expected_in == "OUT":
                return vout
            raise ValueError(f"Unexpected requested position {expected_in}")

        # calculate motions needed
        actual_positions = self._read_positions()
        motions = [compute_motion(ain, ein) for ain, ein in zip(actual_positions, positions)]

        # early skip
        if all([v == vstop for v in motions]):
            return

        try:
            # enable PIEZO controller
            self._enable()

            t_beg = time.time()

            # wait for the end of all motions and stop them one by one
            while any([v != vstop for v in motions]):
                # an actuator may not reach its limitswitch
                if time.time() - t_beg > self.exec_timeout:
                    print("TIMEOUT")
                    log_error(self, f"{self.exec_timeout} sec timeout waiting for end of motion, aborting")
                    self._abort()
                    return

                # update all motions simultaneously
                self.wago.set(self._dac_channel, *motions)

                # do not overload the WAGO box
                time.sleep(1)

                # update the motions needed
                actual_positions = self._read_positions()
                if self.display_during_move:
                    disp_pos = [f"{'' if d else p:<3}" for d, p in zip(self._disabled, actual_positions)]
                    print("\rStatus:", *disp_pos, end="")
                motions = [compute_motion(ain, ein) for ain, ein in zip(actual_positions, positions)]
        except:
            self._abort()
            raise
        finally:
            if self.display_during_move:
                print()
            motions = [vstop] * 20
            self.wago.set(self._dac_channel, *motions)
            self._disable()

    def _status_dict(self):
        """The status of the transfocator as dictionary.

        Returns:
            (dict): Keys are the labels of the lenses, values are True or False
        """
        positions = {}
        values = self._read_positions()
        for i in range(self._nb_actuators):
            if i in self.empty_jacks:
                lbl = f"X{i}"
                position = None
            else:
                lbl = f"P{i}" if i in self.pinhole else f"L{i}"
                position = values[i]
            positions[lbl] = position
        return positions

    def _status_read(self):
        """The status of the transfocator as tuple.

        Returns:
            (tuple): Two strings, where the first contains all the labels and
                     the second - all the positions (IN or OUT)
        """
        header, positions = zip(*self._status_dict().items())
        header = "".join(("{:<4}".format(col) for col in header))
        positions = (_display(col) for col in positions)
        positions = "".join(("{:<4}".format(col) for col in positions))
        return header, positions

    def set(self, *lenses):
        """Set the lenses"""
        status = len(self) * [False]
        for i, lense in enumerate(lenses):
            status[i] = lense
        self[:] = status

    def set_in(self, lense_index):
        """Set a lese in.

        Args:
            (int): The index of the lense.
        """
        self[lense_index] = True

    def set_out(self, lense_index):
        """Set a lese out.

        Args:
            (int): The index of the lense.
        """
        self[lense_index] = False

    def toggle(self, lense_index):
        """Toggle a lense.

        Args:
            (int): The index of the lense.
        """
        self[lense_index] = not self[lense_index]

    def set_n(self, *idx_values):
        """Set the lenses. Check if there is a security pinhole to be set.
            To be used by `__setitem__()`

        Args:
            (list): Lense index, lens value
        """
        positions = [None] * self._nb_actuators
        for idx, value in zip(idx_values[::2], idx_values[1::2]):
            if value is None or idx in self.empty_jacks:
                continue
            else:
                positions[idx] = _encode(value)
        if self.safety and any(positions) and self.pinhole:
            for pinhole in self.pinhole:
                positions[pinhole] = "IN"
        self._move_positions(positions)

    def set_all(self, set_in=True):
        """Set all the lenses IN or OUT
        Args:
           set_in(bool): True for IN, False for OUT
        """
        self[:] = set_in

    def set_pin(self, set_in=True):
        """Put IN or OUT the pinhole(s) only.

        Args:
            set_in(bool): True for IN, False for OUT
        """
        self[self.pinhole] = set_in

    def __state_changed(self, state):
        dispatcher.send("state", self, state)

    def __len__(self):
        return self.nb_lens + self.nb_pinhole

    def __getitem__(self, idx):
        pos = list(self._status_dict().values())
        if isinstance(idx, int):
            return _display(pos[idx])
        if isinstance(idx, slice):
            idx = list(range(*idx.indices(self.nb_lens + self.nb_pinhole)))
        return [_display(pos[i]) for i in idx]

    def __setitem__(self, idx, value):
        if isinstance(idx, int):
            args = idx, value
        else:
            if isinstance(idx, slice):
                idx = list(range(*idx.indices(self.nb_lens + self.nb_pinhole)))
            nb_idx = len(idx)
            if not isinstance(value, (tuple, list)):
                value = nb_idx * [value]
            nb_value = len(value)
            if nb_idx != nb_value:
                raise ValueError(
                    "Mismatch between number of lenses ({}) "
                    "and number of values ({})".format(nb_idx, nb_value)
                )
            args = [val for pair in zip(idx, value) for val in pair]
        self.set_n(*args)

    def __info__(self):
        prefix = "Transfocator " + self.name
        try:
            header, positions = list(zip(*list(self._status_dict().items())))
            positions = [_display(col) for col in positions]
            table = tabulate.tabulate((header, positions), tablefmt="plain")
            return "{}:\n{}".format(prefix, table)
        except Exception as exc:
            raise RuntimeError(
                f"Could not display info for transfocator '{self.name}'"
            ) from exc

    def __str__(self):
        """Channel uses louie behind which calls this object str.

        str is overloaded to avoid calling repr which triggers a connection.
        As the wago hardware controller only accepts a limited number of
        connections, we want to avoid creating a connection just because
        of a louie signal.
        """
        return (
            "<bliss.controllers.transfocator.Transfocator "
            "instance at {:x}>".format(id(self))
        )

    def _abort(self):
        """Abort any motion on all actuators"""
        nb = self.nb_lens + self.nb_pinhole
        values = [self._vstop] * nb
        self.wago.set(self._dac_channel, *values)

    def _enable(self):
        self.wago.set(self._enable_channel, 1, 0)
        time.sleep(0.5)

    def _disable(self):
        self.wago.set(self._enable_channel, 0, 0)

    def _reset(self):
        self.wago.set(self._enable_channel, 1, 0)
        time.sleep(0.2)
        self.wago.set(self._enable_channel, 0, 0)
        time.sleep(0.5)
        self.wago.set(self._enable_channel, 1, 0)

    def pos_read(self) -> int:
        """Read the WAGO position as a bitfield.

        Returns:
           (int): The value, representing the addition of the active bits
        """
        positions = self._read_positions()
        bits = 0
        for i, p in enumerate(positions):
            if i in self.empty_jacks:
                continue
            if p == "IN":
                bits |= 1 << i
        return bits

    def set_bitvalue(self, value):
        """Set bit values checking if there is a security pinhole to set.

        Args:
            (value): lens bit value
        """
        positions = []
        for i in range(self._nb_actuators):
            bit = (value & 1 << i) != 0
            p = "IN" if bit else "OUT"
            if i in self.empty_jacks:
                p = None
            positions.append(p)
        self._move_positions(positions)


class TransfocatorID06Lens:
    def __init__(self, name, config):
        self.name = name

        if "transfocator" not in config:
            raise ValueError(f'Need to specify "transfocator" TFLens [{name}]')
        if "lenses" not in config:
            raise ValueError(f'Need to specify "lenses" for TFLens [{name}]')

        self.__transfocator = config.get("transfocator")

        self._lenses = {}
        lensconfig = config["lenses"]
        for lensdef in lensconfig:
            index = int(lensdef["index"])
            self._lenses[index] = lensdef

        self.ntflens = self.__transfocator.nb_lens + self.__transfocator.nb_pinhole

    @autocomplete_property
    def transfocator(self):
        return self.__transfocator

    def __set_pinhole(self, inout):
        if not self.__transfocator.nb_pinhole:
            raise RuntimeError("Transfocator has no pinhole defined !!")
        self.__transfocator.set_pin(inout)

    def pin(self):
        self.__set_pinhole(True)

    def pout(self):
        self.__set_pinhole(False)

    def setin(self, *lens_ids):
        ids = self.__check_lensid(lens_ids)
        tfset = self.transfocator.pos_read()
        for lid in ids:
            tfset |= 1 << lid
        self.transfocator.set_bitvalue(tfset)

    def setout(self, *lens_ids):
        ids = self.__check_lensid(lens_ids)
        tfset = self.transfocator.pos_read()
        for lid in ids:
            tfset = tfset & (~(1 << lid))
        self.transfocator.set_bitvalue(tfset)

    def __check_lensid(self, lens_ids):
        allids = list(self._lenses.keys()) + self.transfocator.pinhole
        if not len(lens_ids):
            return allids
        for lid in lens_ids:
            if lid not in allids:
                raise ValueError(f"Invalid lens id. Should be in {allids}")
        return lens_ids

    def status(self):
        print(self.__info__())

    def _lenses_state(self):
        def state2string(value):
            states = list()
            for lid in range(self.ntflens):
                if value & (1 << lid):
                    states.append("IN")
                else:
                    states.append("OUT")
            return states

        def get_description(index):
            if index in self.transfocator.pinhole:
                return {
                    "description": "alignment pinhole",
                    "n_r": "",
                }
            return self._lenses.get(index)

        tfstate = self.transfocator.pos_read()
        states = state2string(tfstate)

        for index in range(self.ntflens):
            desc = get_description(index)
            if desc is None:
                continue
            description = desc["description"]
            state = states[index]
            n_r = desc["n_r"]
            yield index, description, n_r, state


    def __info__(self):
        info = ""
        info += "Lense  Description          N/R       Pos\n"
        info += "-----  -------------------  --------  -----\n"
        for desc in self._lenses_state():
            index, description, n_r, state = desc
            if state == "OUT":
                state_s = "  OUT"
            else:
                state_s = state
            line = f"{str(index):>2}     {description:<20} {n_r:<6}    {state_s}"
            if state == "IN":
                info += BOLD(line)
            else:
                info += line
            info += "\n"

        return info


from bliss.shell.cli.user_dialog import (
    UserCheckBoxList,
)
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.shell.dialog.helpers import dialog


@dialog("TransfocatorID06Lens", "selection")
def tranfocator_selection(controller):
    """
    Set-up the magnification for the two objectives mounted.
    They can be chosen from the list of possible magnifications.
    """
    values = []
    defval = []
    for desc in controller._lenses_state():
        index, description, n_r, state = desc
        values.append((index, description))
        if state == "IN":
            defval.append(index)

    dlg = UserCheckBoxList(values=values, defval=defval)
    ret = BlissDialog([[dlg]], title="Transfocator Setup").show()

    if ret is not False:
        transfocator = controller.transfocator
        conf = [False] * len(transfocator)
        for index in ret[dlg]:
            conf[index] = True
        controller.transfocator.set(*conf)
