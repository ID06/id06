from __future__ import annotations

import numpy
import enum
from bliss.controllers.bliss_controller import BlissController
from bliss.common.utils import RED, GREEN, BOLD
from bliss.shell.standard import info


def RED_BOLD_if(text, condition):
    if condition:
        return RED(BOLD(text))
    return text


def GREEN_BOLD_if(text, condition):
    if condition:
        return GREEN(BOLD(text))
    return text


class Id06NanodacLoop:
    """Wrapper of the regulation loop in order to feature ID06 needs"""

    def __init__(self, real_loop):
        # This force the full initialization of the loop
        info(real_loop)

        self.real_loop = real_loop

    @property
    def mode(self) -> str:
        """Returns the mode MANUAL/AUTO"""
        loop = self.real_loop
        channel = loop.channel
        is_auto = loop.controller.hw_controller.is_auto(channel)
        if is_auto:
            return "AUTO"
        else:
            return "MANUAL"

    @property
    def is_auto(self):
        return self.mode == "AUTO"

    @property
    def is_manual(self):
        return self.mode == "MANUAL"

    def manual_mode(self):
        loop = self.real_loop
        channel = loop.channel
        loop.controller.hw_controller.set_auto(channel, False)

    def auto_mode(self):
        loop = self.real_loop
        channel = loop.channel
        loop.controller.hw_controller.set_auto(channel, True)

    @property
    def power(self):
        out = self.real_loop.output
        return out.read()

    @power.setter
    def power(self, power):
        loop = self.real_loop
        channel = loop.channel

        self.manual_mode()

        #hw = loop.controller.hw_controller
        #if power <= 95:
        #    max_power = power + power * 0.05
        #else:
        #    max_power = 100
        #hw.send_cmd(f"Loop_{channel}_OP_OutputHighLimit", max_power)
        # hw.send_cmd(f"Loop_PID_GainScheduledOutputHighLimit", max_power)

        out = self.real_loop.output
        out.controller.set_output_value(out, power)

    @property
    def high_limit(self):
        loop = self.real_loop
        channel = loop.channel
        hw = loop.controller.hw_controller
        return hw.send_cmd(f"Loop_{channel}_OP_OutputHighLimit")

    @high_limit.setter
    def high_limit(self, value):
        loop = self.real_loop
        channel = loop.channel
        hw = loop.controller.hw_controller
        return hw.send_cmd(f"Loop_{channel}_OP_OutputHighLimit", value)

    @property
    def low_limit(self):
        loop = self.real_loop
        channel = loop.channel
        hw = loop.controller.hw_controller
        return hw.send_cmd(f"Loop_{channel}_OP_OutputLowLimit")

    @low_limit.setter
    def low_limit(self, value):
        loop = self.real_loop
        channel = loop.channel
        hw = loop.controller.hw_controller
        return hw.send_cmd(f"Loop_{channel}_OP_OutputLowLimit", value)

    @property
    def sensor_break_mode(self):
        """Returns the sensor break mode"""
        loop = self.real_loop
        channel = loop.channel
        mode = loop.controller.hw_controller.send_cmd(
            f"Loop_{channel}_OP_SensorBreakMode"
        )
        if mode == 0:
            return "SbrkOP"
        elif mode == 1:
            return "Hold"
        else:
            return str(mode)

    def sensor_break_sbrkop(self):
        """Switch the sensor break mode to HOLD"""
        loop = self.real_loop
        channel = loop.channel
        loop.controller.hw_controller.send_cmd(f"Loop_{channel}_OP_SensorBreakMode", 0)

    def sensor_break_hold(self):
        """Switch the sensor break mode to HOLD"""
        loop = self.real_loop
        channel = loop.channel
        loop.controller.hw_controller.send_cmd(f"Loop_{channel}_OP_SensorBreakMode", 1)

    @property
    def sensor_break_type(self):                                                                  
        hw = self.real_loop.controller.hw_controller
        t = hw.send_cmd("Channel_1_Main_SensorBreakType")
        if t == 0:                                                                            
            return "DISABLED"                                                                 
        elif t == 1:                                                                          
            return "LOW"                                                                      
        elif t == 2:                                                                          
            return "HIGH"                                                                     
        return str(t)                                                                         

    @property
    def sensor_break_val(self):                                                                  
        hw = self.real_loop.controller.hw_controller
        return hw.send_cmd("Channel_1_Main_SensorBreakVal")

    def plot(self):
        self.real_loop.plot()

    @property
    def setpoint(self):
        return self.real_loop.setpoint

    @setpoint.setter
    def setpoint(self, value):
        self.real_loop.setpoint = value

    @property
    def ramprate(self):
        return self.real_loop.ramprate

    @ramprate.setter
    def ramprate(self, value):
        self.real_loop.ramprate = value

    def __info__(self):
        mode = self.mode

        result = ""
        result += "ID06NanodacLoop\n"
        result += "\n"
        result += f"real loop:     {self.real_loop.name}\n"
        result += "\n"
        result += f"mode:          {self.mode}\n"
        result += "\n"
        result += RED_BOLD_if("manual:\n", mode == "MANUAL")
        result += f"   power:      {self.power:10.3f}\n"
        result += f"   limits:{self.low_limit:10.3f} ... {self.high_limit:<10.3f}\n"
        result += "\n"
        result += GREEN_BOLD_if("auto:\n", mode == "AUTO")
        result += f"   setpoint:   {self.setpoint:10.3f}\n"
        result += f"   ramprate:   {self.ramprate:10.3f}\n"
        result += "\n"
        result += "sensor break:\n"
        result += f"   mode:       {self.sensor_break_mode}\n"
        return result
