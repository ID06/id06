from bliss.common.counter import IntegratingCounter
from bliss.controllers.counter import SamplingCounterController
from bliss.controllers.bliss_controller import BlissController
from bliss.comm.taco.client import Client
from bliss.controllers.counter import IntegratingCounterController
from bliss.controllers.counter import IntegratingCounterAcquisitionSlave


class _P201TacoCounterAcquisitionSlave(IntegratingCounterAcquisitionSlave):

    def prepare_device(self):
        self._device._taco.DevCntPresetValue(int(self.count_time * 1000000))

    def start_device(self):
        self._device._taco.DevCntStart()

    def stop_device(self):
        # self._device._taco.DevCntStop()
        pass


class _P201TacoCounterController(IntegratingCounterController):
    def __init__(self, name, controller, taco_name, taco_host):
        super().__init__(name)
        self.controller = controller
        self.__taco = Client(taco_name, db_host=taco_host)
        # self._taco.connect()

    @property
    def _taco(self):
        return self.__taco

    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        return _P201TacoCounterAcquisitionSlave(
            self, ctrl_params=ctrl_params, **acq_params
        )

    def get_default_chain_parameters(self, scan_params, acq_params):
        try:
            count_time = acq_params["count_time"]
        except KeyError:
            count_time = scan_params["count_time"]

        params = {
            "count_time": count_time,
            "npoints": 1,
        }

        return params

    def get_values(self, from_index, *counters):
        """
        Return the values of the given counters as a list.
        """
        assert from_index == 0
        status = self.__taco.DevCntStatus()
        if status != 0:
            result = [[]] * len(counters)
        else:
            raw_data = self.__taco.DevCntReadAll()
            data = {}
            for i, v in zip(raw_data[0::2], raw_data[1::2]):
                data[i] = v
            result = [[data[c.address]] for c in counters]
        return result

    def read(self, counter):
        return self.get_values(counter)


class P201TacoController(BlissController):

    def __init__(self, config):
        super().__init__(config)
        self._counter_controller = _P201TacoCounterController(
            name=self.name,
            controller=self,
            taco_name=config.get("taco_name"),
            taco_host=config.get("taco_host"),
        )

    @property
    def counters(self):
        return self._counter_controller.counters

    def _load_config(self):
        for cfg in self.config["counters"]:
            self._get_subitem(cfg["name"])

    def _get_subitem_default_class_name(self, cfg, parent_key):
        if parent_key == "counters":
            return "IntegratingCounter"
        raise RuntimeError(f"Key '{parent_key}' unsupported")

    def _create_subitem_from_config(self, name, cfg, parent_key, item_class, item_obj=None):
        if parent_key == "counters":
            name = cfg["name"]
            address = cfg["address"]
            if address < 1 or address > 12:
                raise RuntimeError(f"Counter {name} have a wrong address '{address}'")
            unit = cfg.get("unit")
            cnt = item_class(name, self._counter_controller, unit=unit)
            cnt.address = address
            return cnt

        raise RuntimeError(f"Key '{parent_key}' unsupported")
