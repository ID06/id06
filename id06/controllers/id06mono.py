
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
import tabulate
import gevent
import time
import glob


from bliss.shell.standard import umv
from bliss.common.utils import ColorTags, BOLD, RED, ORANGE, BLUE, GREEN
from bliss.controllers.monochromator.monochromator import MonochromatorFixExit

class ID06mono(MonochromatorFixExit):

    def __info__(self):
        print(f"\n{self._get_info_mono()}")
        print(f"\n{self._get_info_xtals()}")
        print(f"\n{self._get_info_motors()}")
        return ""

    """
    Xtals
    """
    def _xtal_is_in(self, xtal):
        change_motor = self._xtals.get_xtals_config("change_xtal_motor")[xtal]
        current_pos = change_motor.position
        if current_pos > self._xtals.get_xtals_config("low_pos")[xtal] and current_pos < self._xtals.get_xtals_config("high_pos")[xtal]:
            return True
        return False
       
    def _xtal_change(self, xtal):
        if not self._xtal_is_in(xtal):
            # Put xtal in the beam
            change_motor = self._xtals.get_xtals_config("change_xtal_motor")[xtal]
            target_pos = self._xtals.get_xtals_config("change_xtal_position")[xtal]
            umv(change_motor,target_pos)

    """
    TRAJECTORY
    """
    def _get_bragg_trajectory(self, ene_data):
        raise NotImplementedError("Should be implemented taling into account dxtal motor")        
