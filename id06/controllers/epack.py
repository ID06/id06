from bliss.common.counter import IntegratingCounter
from bliss.controllers.counter import SamplingCounterController
from bliss.controllers.bliss_controller import BlissController
from bliss.controllers.counter import IntegratingCounterController
from bliss.controllers.counter import IntegratingCounterAcquisitionSlave
from bliss.comm.modbus import ModbusTCP


class _EpackCounterAcquisitionSlave(IntegratingCounterAcquisitionSlave):

    def prepare_device(self):
        pass

    def start_device(self):
        pass

    def stop_device(self):
        pass


class _EpackCounterController(IntegratingCounterController):
    def __init__(self, name, controller, host, port):
        super().__init__(name)
        self.controller = controller
        self.__modbus = ModbusTCP(url=f"{host}:{port}")

    @property
    def _modbus(self):
        return self.__modbus

    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        return _EpackCounterAcquisitionSlave(
            self, ctrl_params=ctrl_params, **acq_params
        )

    def get_default_chain_parameters(self, scan_params, acq_params):
        try:
            count_time = acq_params["count_time"]
        except KeyError:
            count_time = scan_params["count_time"]

        params = {
            "count_time": count_time,
            "npoints": 1,
        }

        return params

    def get_values(self, from_index, *counters):
        """
        Return the values of the given counters as a list.
        """
        assert from_index == 0
        return [self.read(c) for c in counters]

    def read(self, counter):
        scaling_factor = counter.scaling_factor
        address = counter.address
        return [self.__modbus.read_holding_registers(address, "H") * scaling_factor]


class EpackController(BlissController):

    def __init__(self, config):
        super().__init__(config)
        self._counter_controller = _EpackCounterController(
            name=self.name,
            controller=self,
            host=config.get("host"),
            port=config.get("port", 502),
        )

    @property
    def counters(self):
        return self._counter_controller.counters

    def _load_config(self):
        for cfg in self.config["counters"]:
            self._get_subitem(cfg["name"])

    def _get_subitem_default_class_name(self, cfg, parent_key):
        if parent_key == "counters":
            return "IntegratingCounter"
        raise RuntimeError(f"Key '{parent_key}' unsupported")

    def _create_subitem_from_config(self, name, cfg, parent_key, item_class, item_obj=None):
        if parent_key == "counters":
            name = cfg["name"]
            address = cfg["address"]
            scaling_factor = cfg.get("scaling_factor", 1)
            unit = cfg.get("unit")
            cnt = item_class(name, self._counter_controller, unit=unit)
            cnt.address = address
            cnt.scaling_factor = scaling_factor
            return cnt

        raise RuntimeError(f"Key '{parent_key}' unsupported")
