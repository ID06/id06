from bliss.setup_globals import *
import numpy as np
import sys
import time
import glob
import os
from bliss import current_session



def saveMotors():
    from bliss.common.standard import iter_axes_position_all
    from bliss.common.utils import rounder
    """
    Sava all motor positions in both user and dial units
    """
    nowStr=time.strftime("%Y%m%d-%Hh%M")
    outDir="/data/id06-hxm/inhouse/spec2bliss/"
    outFile="%s/%s_motorPos_%s.txt" % (outDir,SCAN_SAVING.session,nowStr)
    print("Positions will be saved to file: %s" % outFile)
    
    header, pos, dial = [], [], []
    tables = [(header, pos, dial)]
    errors = []
    data = iter_axes_position_all()
    with open(outFile,"w") as outf:
        outf.write("#motor        user       dial\n")
        for axis, disabled, error, axis_unit, position, dial_position in data:
            mytolerance=min(0.00001,axis.tolerance*0.1)
            if disabled:
                print(axis.name, position, dial_position, "DISABLED")
                outf.write("%10s %10.5f %10.5f DISABLED\n" % (axis.name,position,dial_position))
            elif error:
                print(axis.name, position, dial_position, "ERROR")
                try:
                   outf.write("%10s %10.5f %10.5f ERROR\n" % (axis.name,position,dial_position))
                except:
                   outf.write("%10s - -  ERROR\n" % (axis.name))
            else:
                print(axis.name, rounder(mytolerance,position), rounder(mytolerance,dial_position))
                outf.write("%10s %10s %10s\n" % (axis.name,rounder(mytolerance,position), rounder(mytolerance,dial_position)))

      
def returnMotors(filename=None,doMove=False,verbose=False):
    from bliss.common.standard import iter_axes_position_all
    from bliss.common.utils import rounder
    """
    Return all motor positions to previous positions
    """
    inDir="/data/id06-hxm/inhouse/spec2bliss/"
    if filename==None:
        #find youngest file
        list_of_files = glob.glob("%s/*%s*.txt" % (inDir,SCAN_SAVING.session))
        filename=max(list_of_files, key=os.path.getctime)
    else:
        if "/" in filename:
            pass
        else:
            filename="%s/%s" % (inDir,filename)
        
    #inFile="%s/%s_motorPos_%s.txt" % (inDir,filename)
    print("Positions will be recovered from file: %s" % filename)
    with open(filename) as inf:
        lines=inf.readlines()

    #current positions
    motorsNow = iter_axes_position_all()
    foundIssue=False
    Nmotors=0
    for axis, disabled, error, axis_unit, position, dial_position in motorsNow:
        Nmotors+=1
        #find good line in file
        ii=0
        mytolerance=min(axis.tolerance,0.00001)
        while lines[ii].split()[0]!=axis.name:
            ii+=1
        line=lines[ii]
        line=line.rstrip("\n")
        if "DISABLED" in line:
            print("%s was DISABLED. now DISABLE=%s" % (axis.name,disabled))
            print("Skip motor")
            foundIssue=True
        elif "ERROR" in line:
            print("%s was ERROR. now ERROR=%s" % (axis.name,error))
            print("Skip motor")
            foundIssue=True
        else:
            mymot=line.split()[0]
            mypos=float(line.split()[1])
            mydial=float(line.split()[2])
            if verbose:
                print(mymot,mypos,mydial)
                print("%10s     was     is" % axis.name)
                print("User    %s      %s" % (line.split()[1],rounder(mytolerance,position))) 
                print("Dial    %s      %s" % (line.split()[2],rounder(mytolerance,dial_position)))
            #comparing
            if rounder(mytolerance,mypos)==rounder(mytolerance,position) and rounder(mytolerance,mydial)==rounder(mytolerance,dial_position):
                if verbose:
                    print("no move needed !")
            elif abs(float(rounder(mytolerance,mypos ))-float(rounder(mytolerance,position)))==abs(float(rounder(mytolerance,mydial ))-float(rounder(mytolerance,dial_position))):
                deltaPos =float(rounder(mytolerance,mypos ))-float(rounder(mytolerance,position))
                deltaDial=float(rounder(mytolerance,mydial))-float(rounder(mytolerance,dial_position))
                print("MOTOR %s WAS MOVED" % axis.name)
                print("        was       is         delta")
                print("User    %s      %s      %f" % (line.split()[1],rounder(mytolerance,position),deltaPos)) 
                print("Dial    %s      %s      %f" % (line.split()[2],rounder(mytolerance,dial_position),deltaDial))
                print("umv(%s,%f)" % (axis.name,mypos))
                foundIssue=True
                if doMove:
                    umv(axis,mypos)
            else:
                print("MOTOR %s WAS MOVED" % axis.name)
                deltaPos =float(rounder(mytolerance,mypos ))-float(rounder(mytolerance,position))
                deltaDial=float(rounder(mytolerance,mydial))-float(rounder(mytolerance,dial_position))
                print("        was       is         delta")
                print("User    %s      %s      %f" % (line.split()[1],rounder(mytolerance,position),deltaPos)) 
                print("Dial    %s      %s      %f" % (line.split()[2],rounder(mytolerance,dial_position),deltaDial))
                print("Dial and User values have been shifted/moved by different amounts!!!")
                print("I CAN NOT RESOLVE THIS!")
                foundIssue=True
    if foundIssue==False:
        print("checked on %d motors. No issue found" % Nmotors)
                
