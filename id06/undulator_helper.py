from bliss.common.utils import BOLD
import numpy

U18 = {}
U18["NAME"]      = "u18"      # name of undulator
U18["MOTOR_MNE"] = "u18u"     # motor mnemonic
U18["ID_NAME"]   = "CPU18-1A_GAP" # name used for ID_TANGO
U18["B0"]        = 2.34       # Tesla 
U18["LAMBDA0"]   = 18.34      # mm 
U18["ALPHA"]     = 1.14
U18["HARMONIC"]  = 1.0        # default harmonic
U18["GAMMA"]     = 11742.0    # machine parameter, fixed
U18["GAP_MIN"]   = 6.0        # minimum gap, heat-load limited
U18["GAP_MAX"]   = 30.0       # maximum gap

U27 = {}
U27["NAME"]      = "u27"      # name of undulator
U27["MOTOR_MNE"] = "u27d"     # motor mnemonic
U27["ID_NAME"]   = "U27C_GAP" # name used for ID_TANGO
U27["B0"]        = 2.10       # Tesla 
U27["LAMBDA0"]   = 26.73      # mm
U27["ALPHA"]     = 1.0        #   
U27["HARMONIC"]  = 3.0        # default harmonic
U27["GAMMA"]     = 11742.0    # machine parameter, fixed
U27["GAP_MIN"]   = 11.0       # min. gap
U27["GAP_MAX"]   = 253.0      # max. gap

U35 = {}
U35["NAME"]      = "u35"      # name of undulator
U35["MOTOR_MNE"] = "u34d"     # motor mnemonic (sic)
U35["ID_NAME"]   = "U35C_GAP" # name used for ID_TANGO
U35["B0"]        = 1.936      # Tesla 
U35["LAMBDA0"]   = 35.35      # mm
U35["ALPHA"]     = 1.027      #  
U35["HARMONIC"]  = 1.0        # default harmonic
U35["GAMMA"]     = 11742.0    # machine parameter, fixed
U35["GAP_MIN"]   = 11.0       # min. gap
U35["GAP_MAX"]   = 253.0      # max. gap


UNDULATORS = [U18, U27]


HC_OVER_E = 12.3984


def _u_calc_gap(energy, und, silent: bool = False):
    """
    Calculate gap as function of undulator and machine parameters

    Arguments:
        energy: photon energy
        und: array of undulator parameters
        silent: False: print error messages, True: print nothing.
    """
    if energy <= 0.0:
        if not silent:
            print("Sorry, can only deal with positive energies")
        return -1.0
    if und["HARMONIC"] <= 0.0:
        if not silent:
            print("Sorry, can only deal with positive harmonics")
        return -1.0
    if und["GAMMA"] <= 0.0:
        if not silent:
            print("Sorry, can only deal with positive machine energies")
        return -1.0

    lambd = und["HARMONIC"] * HC_OVER_E / energy
    k2 = 4.0e-7 * und["GAMMA"]**2 * lambd / und["LAMBDA0"] - 2.0
    if k2 < 0.0:
        if not silent:
            print(f"Error: Too high energy ({energy}) for this harmonic ({und['HARMONIC']}")
        return -1.0

    k = numpy.sqrt(k2)
    gap = -und["LAMBDA0"] / und["ALPHA"] / numpy.pi * numpy.log(k / 0.0934 / und["LAMBDA0"] / und["B0"])
    if gap < und["GAP_MIN"]:
        if not silent:
           print(f"Error: Too high energy ({energy}) for this harmonic ({und['HARMONIC']}), calculated gap is {gap}")
        return -1.0
    if gap > und["GAP_MAX"]:
        if not silent:
           print(f"Error: Too high energy ({energy}) for this harmonic ({und['HARMONIC']}), calculated gap is {gap}")
        return -1.0

    return gap


def _u_listoptions(energy, und, silent: bool = False, odd: bool = True):
    """
    List options for a given undulator and photon energy.

    Arguments:
        energy: photon energy
        und: undulator characteristics
        silent: do not print messages
        odd: select odd harmonics only
    """
    old_harm = und["HARMONIC"]
    found = -1
    gap = und["GAP_MAX"]
    name = und["NAME"]

    if not silent:
        print(BOLD(f"Possible gaps for {name} at {energy:6.3f} keV"))
    for harm in range(1, 11):
        if not odd and (harm % 2 == 0):
            continue
        und["HARMONIC"] = harm
        gap = _u_calc_gap(energy, und, -1)
        if gap != -1:
            found = gap
            if not silent:
                print(f"  Use harmonic {harm} at gap {found:3.2f}")
    und["HARMONIC"] = old_harm
    if found == -1:
        if not silent:
            print("  No good harmonic found")

    if not silent:
        print()

    return found


def uoptions(energy=None, odd=None):
    """Display possible undulator settings for given photon energy, with possible preference for odd harmonics
    """
    for und in UNDULATORS:
        _u_listoptions(energy, und, False, odd)
