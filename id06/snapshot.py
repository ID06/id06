

"""
???
"""


import pprint

from bliss.config.settings import ParametersWardrobe
from bliss.config.settings import SimpleSetting
from bliss.shell.standard import umv, umvd, umvr, mvd, mvr, _move, __row_positions, __row, ct
from bliss import global_map
from bliss.scanning.toolbox import ChainBuilder
from bliss.common.axis import Axis, AxisOnLimitError
from bliss import current_session
from bliss.common.measurementgroup import get_active as get_active_mg
from tabulate import tabulate
from bliss.controllers.multiplepositions import MultiplePositions
from tango import ConnectionFailed
try:
    from tomo.optic.twinmic_optic import TwinOptic
except ImportError:
    TwinOptic = "TwinOptic"
    tomo = None

from bliss.shell.cli.user_dialog import (
    UserIntInput,
    UserFloatInput,
    UserCheckBox,
    UserChoice,
    Container,
    UserMsg,
    Validator,
    UserInput,
)
from bliss.common.utils import grouped
from bliss.common.types import (  # noqa: F401
    _countable,
    _scannable_or_name,
    _float,
    _scannable_position_list,
    _scannable_position_list_or_group_position_list,
    _providing_channel,
)
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.common.cleanup import cleanup
import re, datetime, ast
from datetime import datetime
from tinydb import TinyDB, Query
import string, random
from bliss import setup_globals


def set_int_accumulation(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('ACCUMULATION')
    det_trig.set('INTERNAL_TRIGGER')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.MRTOMO_int_chain_acc["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        setup_globals.DEFAULT_CHAIN.set_settings(
            setup_globals.MRTOMO_int_chain["chain_config"]
        )
        raise Exception(
            f"{detector.name} is not configured in default chain for accumulation"
        )


def set_int_trigger(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('SINGLE')
    det_trig.set('INTERNAL_TRIGGER')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.MRTOMO_int_chain["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f"{detector.name} is not configured in default chain")

def test_date(c_date, days):
    date_format = '%Y-%m-%d %H:%M:%S'
    return (datetime.now() - datetime.strptime(c_date, date_format)).days <= days


def between_date(c_date, date1, date2):
    simple_format = '%d/%m/%Y'
    date_format = '%Y-%m-%d %H:%M:%S'
    date1 = datetime.strptime(date1, simple_format)
    date2 = datetime.strptime(date2, simple_format)
    c_date = datetime.strptime(c_date, date_format)
    if c_date <= date2 and c_date >= date1:
        return True
    else:
        return False

class Snap:
    """

    """
    def __init__(self, database):
        self._database = database
        self.name = None
        self.comment = None
        self.creation_date = None
        self.proposal = None
        self.slits = {}
        self.attenuators = {}
        self.tomo = {}
        self.detector = {}
        self.motors = {}
        self.apply_attenuators = None
        self.apply_slits = None
        self.apply_motors = None
        self.apply_tomo = None
        self.apply_detector = None
        self.query = Query()

    def __info__(self):
        return f"{self.name} {self.creation_date} {self.proposal}"

    def switch(self, name):
        query = Query()
        snap_dict = self._database.search(query.name == name)[0]
        self.name = snap_dict["name"]
        self.comment = snap_dict["comment"]
        self.proposal = snap_dict["proposal"]
        self.creation_date = snap_dict["creation_date"]
        self.slits = snap_dict["slits"]
        self.attenuators = snap_dict["attenuators"]
        if tomo:
            self.tomo = snap_dict["tomo"]
        self.detector = snap_dict["detector"]
        self.motors = snap_dict["motors"]
        self.apply_attenuators = snap_dict["apply_attenuators"]
        self.apply_slits = snap_dict["apply_slits"]
        self.apply_motors = snap_dict["apply_motors"]
        self.apply_tomo = snap_dict["apply_tomo"]
        self.apply_detector = snap_dict["apply_detector"]

    def update(self):
        query = Query()
        snap_dict = self._database.search(query.name == self.name)[0]
        self.name = snap_dict["name"]
        self.comment = snap_dict["comment"]
        self.proposal = snap_dict["proposal"]
        self.creation_date = snap_dict["creation_date"]
        self.slits = snap_dict["slits"]
        self.attenuators = snap_dict["attenuators"]
        if tomo:
            self.tomo = snap_dict["tomo"]
        self.detector = snap_dict["detector"]
        self.motors = snap_dict["motors"]
        self.apply_attenuators = snap_dict["apply_attenuators"]
        self.apply_slits = snap_dict["apply_slits"]
        self.apply_motors = snap_dict["apply_motors"]
        self.apply_tomo = snap_dict["apply_tomo"]
        self.apply_detector = snap_dict["apply_detector"]

    def to_dict(self):
        query = Query()
        snap_dict = self._database.search(query.name == self.name)[0]
        return snap_dict

    def from_dict(self, snap_dict):
        query = Query()
        self._database.update(snap_dict, query.name == self.name)
        self.update()

class multiRes:
    """
    """
    def __init__(self, name, config):
        self.__config = config

        self.shutter = config.get("shutter",None)
        self.frontend = config.get("frontend",None)
        db = config.get("database")

        # Creates attenuators object to handle attenuator positions
        self._attenuators = attenuatorsSnap(name, config)

        # Creates slits object to handle slit positions
        self._slits = slitsSnap(name, config)

        # Creates motors object to handle motor positions
        self._motors = motorsSnap(name, config)

        # self.optics = opticsSnap(name, config)

        if tomo is not None:
            self._tomo = tomoSnap(name, config)
        else:
            self._tomo = None

        self._detector = detectorSnap(name, config)

        # self.snapshots = ParametersWardrobe('default')
        self._database = TinyDB(db, sort_keys=False, indent=4, separators=(',', ': '))
        self.snapshot = Snap(self._database)
        self.query = Query()
        s_list = [snap["name"] for snap in self._database.all()]

        # cg 2024_02_04 try to purge the list.
        for ii in s_list:
            if isinstance(ii, int):
                s_list.remove(ii)

        s_list.sort()
        self._names = s_list
        self._init_default()
        self._last_selected = None

    def save(self, name=None, comment="None"):
        """
        Method to save the beamline state in the database
        """

        if name is None:
            name = self._menu_snap_name()
            if name is None:
                return

        if not isinstance(name,str):
            raise ValueError("snapshot name should be a string")

        # save attenuators positions
        attenuators_dict = self._attenuators.save()
        # save slits positions
        slits_dict = self._slits.save()
        # save motors positions
        motors_dict = self._motors.save()
        if self._tomo is not None:
            # save tomo parameters
            tomo_dict = self._tomo.save()
        # save detector parameters
        detector_dict = self._detector.save()

        snapshot_dict = dict()
        snapshot_dict["name"] = name
        snapshot_dict["creation_date"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        scan_saving = current_session.scan_saving
        proposal_name = scan_saving.proposal_name
        snapshot_dict["proposal"] = proposal_name
        snapshot_dict["comment"] = comment
        snapshot_dict["attenuators"] = attenuators_dict
        snapshot_dict["slits"] = slits_dict
        snapshot_dict["motors"] = motors_dict
        # snapshot_dict["optics"]      =  optics_dict
        if self._tomo is not None:
            snapshot_dict["tomo"] = tomo_dict
        snapshot_dict["detector"] = detector_dict
        # self.snapshots.from_dict(snapshot_dict)
        if self._database.search(self.query.name == name):
            self._database.update(snapshot_dict, self.query.name == name)
        else:
            snapshot_dict["apply_attenuators"] = True
            snapshot_dict["apply_slits"] = True
            snapshot_dict["apply_motors"] = True
            snapshot_dict["apply_tomo"] = self._tomo is not None
            snapshot_dict["apply_detector"] = True
            self._database.insert(snapshot_dict)
            s_list = [snap["name"] for snap in self._database.all()]
            s_list.sort()
            self._names = s_list
        self._last_selected = (snapshot_dict["name"],snapshot_dict["name"])
        self.snapshot.switch(name)

        # BM05 specific
        if hasattr(setup_globals, "logbook"):
            setup_globals.logbook()

    def save_from(self, name, snapshot_dict):
        if len(self._database.search(self.query.name == name)) > 0:
            self._database.update(snapshot_dict, self.query.name == name)
        else:
            self._database.insert(snapshot_dict)
        self.snapshot.switch(name)

    def clean(self, name=None):

        if name is None:
            name = self._menu_snap_name()
            if name is None:
                return

        if name in self._names:
            snapshot_dict = self._database.search(self.query.name == name)[0]
            if self._tomo is not None:
                snapshot_dict["tomo"] = self._tomo.clean(self.snapshot["tomo"])
            snapshot_dict["detector"] = self._detector.clean(self.snapshot["detector"])
            self.save_from_dict(name, snapshot_dict)

    def _menu_snap_name(self):
        """
        UI method to select a snapshot name from a list
        """
        name_date = [snap["name"] + '('.rjust(75-len(snap["name"])) +  snap["creation_date"] + ')' for snap in self._database.all()]

        value_list = [(x, x) for x in name_date]
        value_list.sort()
        ret = True
        choice = "NAME"

        if self._last_selected is None:
            default = value_list.index(value_list[-1])
        else:
            try:
                default = value_list.index(self._last_selected)
            except ValueError:
                default = value_list.index(value_list[-1])

        while ret:
            dlg1 = UserChoice(values=value_list, defval=default)
            ret = BlissDialog([[dlg1]], title="Tomo Setup").show()

            # returns False on cancel
            if ret != False:
                choice = ret[dlg1]
                self._last_selected = (choice, choice)
                return str(choice.split(' ')[0])

    def apply_config(self, name= None):

        if name is None:
            name = self._menu_snap_name()
            if name is None:
                return

        if name in self._names:
            snapshot_dict = self._database.search(self.query.name == name)[0]

        if snapshot_dict.get("apply_attenuators") is None:
            snapshot_dict["apply_attenuators"] = True
            snapshot_dict["apply_slits"] = True
            snapshot_dict["apply_motors"] = True
            snapshot_dict["apply_tomo"] = True
            snapshot_dict["apply_detector"] = True

        attenuators = UserCheckBox(label="attenuators", defval=snapshot_dict["apply_attenuators"])
        slits = UserCheckBox(label="slits", defval=snapshot_dict["apply_slits"])
        motors = UserCheckBox(label="motors", defval=snapshot_dict["apply_motors"])
        tomo = UserCheckBox(label="tomo", defval=snapshot_dict["apply_tomo"])
        detector = UserCheckBox(label="detector", defval=snapshot_dict["apply_detector"])

        ret = BlissDialog( [[attenuators], [slits], [motors], [tomo], [detector]], title="Apply config").show()

        if ret != False:
            snapshot_dict["apply_attenuators"] = ret[attenuators]
            snapshot_dict["apply_slits"] = ret[slits]
            snapshot_dict["apply_motors"] = ret[motors]
            snapshot_dict["apply_tomo"] = ret[tomo]
            snapshot_dict["apply_detector"] = ret[detector]
            self._database.update(snapshot_dict, self.query.name == name)
            self.snapshot.switch(name)

    def apply(self, name=None, shutter=True, frontend=True, slits=True, attenuators=True, motors=True, tomo=True, detector=True):
        """
        Method to apply a beamline state from the database
        """
        if name is None:
            name = self._menu_snap_name()
            if name is None:
                return

        try:
            if shutter and self.shutter:
                self.shutter.close()
            if frontend and self.frontend:
                self.frontend.close()
        except Exception as e:
            print(f"There was an issue to close the shutter or frontend\n {e}")

        if name in self._names:
            # apply attenuators positions
            snapshot_dict = self._database.search(self.query.name == name)[0]

            if snapshot_dict["apply_detector"] and "detector" in snapshot_dict and "detector" in snapshot_dict["detector"]:
                # check if the detector is ready
                try:
                    detector_name = snapshot_dict["detector"]["detector"]["name"]
                    config = current_session.config
                    det = config.get(detector_name)
                    det.detector.camera_type
                except (ConnectionFailed,AttributeError):
                    print(f"The detector {detector_name} is not READY")
                    return

            args = []
            if snapshot_dict["apply_attenuators"]:
                print("Applying attenuators ...")
                attenuators_dict = snapshot_dict["attenuators"]
                latt = self._attenuators.apply(attenuators_dict)
                args.extend(latt)
                #self.bm_move(*latt)
                #print(attenuators_dict)

            # apply slits positions
            if snapshot_dict["apply_slits"]:
                print("Applying slits ...")
                slits_dict = snapshot_dict["slits"]
                lslits = self._slits.apply(slits_dict)
                args.extend(lslits)
                #self.bm_move(*lslits)
                #print(slits_dict)

            # apply motors positions
            if snapshot_dict["apply_motors"]:
                print("Applying motors ...")
                motors_dict = snapshot_dict["motors"]
                #print(self.snapshots.motors)
                #self.motors.apply(motors_dict)
                # apply only the positions of one motor group
                if 'sample_position' in motors_dict:
                    lmotors = self._motors.apply(motors_dict, 'sample_position')
                    args.extend(lmotors)
                    #self.bm_move(*lmotors)
                if 'detector_position' in motors_dict:
                    lmotors = self._motors.apply(motors_dict, 'detector_position')
                    args.extend(lmotors)
                    #self.bm_move(*lmotors)

            # apply optic setup
            #optics_dict = self.snapshots.optics
            #print(self.snapshots.optics)
            #self.optics.apply(optics_dict)

            # apply tomo setup
            if self._tomo and snapshot_dict["apply_tomo"]:
                print("Applying tomo setup ...")
                tomo_dict = snapshot_dict["tomo"]
                self._tomo.apply(tomo_dict)

            # apply detector setup
            if self._tomo and snapshot_dict["apply_detector"]:
                print("Applying detector and optic ...")
                detector_dict = snapshot_dict["detector"]
                ldet = self._detector.apply(detector_dict)
                args.extend(ldet)
                #self.bm_move(*ldet)

            if len(args) > 0:
                self.bm_move(*args)
            if self._tomo and snapshot_dict["apply_motors"]:
                tomo_dict = snapshot_dict["tomo"]
                for mot in self._motors.motors_list["sample_position"]["motors"]:
                    for ref_mot in tomo_dict["reference"]["motors"]:
                        if mot.name in ref_mot:
                            mot.position = motors_dict["sample_position"][mot.name]["position"]
        else:
            print("Unknown snapshot name!")

        self.snapshot.switch(name)
        if frontend and self.frontend:
            self.frontend.open()

        print("The snapshot was applied")
        # setup_globals.update_on()
        # tomo_dict = snapshot_dict["tomo"]
        # ct(tomo_dict["config_pars"]["exposure_time"])


    def remove(self, name=None):
        """
        Method to remove a snapshot from the database
        """
        if name is None:
            name = self._menu_snap_name()
            if name is None:
                return
        self._database.remove(self.query.name == name)
        self._names.remove(name)

    def _init_default(self):
        self.snapshot = Snap(self._database)

    def bm_move(self,*args):
        l = [i for i in args]
        if len(l) == 0:
            return
        objects = [l[i] for i in range(0,len(l),2)]
        girder = None
        # c = None
        # for i in range(0, len(l), 2):
            # if "girderx" in l[i].name:
                # c = i
        # if c!= None:
            # girder = [l[c], l[c+1]]
            # l.pop(c+1)
            # l.pop(c)
            # objects.pop(int(c/2))

        with cleanup(*objects):
            while True:
                try:
                    umv(*args)
                    break
                except AxisOnLimitError as e:
                    a = e.__str__().split(":")[0]
                    for e in l:
                        if a == e.name:
                            c = list(args).index(e)
                        list(args).pop(c+1)
                        list(args).pop(c)

    def edit_snapshots(self, name=None):
        if name is None:
            name = self._menu_snap_name()
            if name is None:
                return
        current_snap = self.snapshot.name
        self.snapshot.switch(name)

        value_list = [
            ("description", "Description"),
            ("attenuators", "Attenuators"),
            ("slits", "Slits"),
            ("motors", "Motors"),
            ("sequence", "Tomo sequence and reference"),
            ("detector", "Detector, Optic and counters"),
        ]

        value_list.append(("exit", "Exit"))

        ret = True
        choice = "scan"
        default = value_list.index(value_list[-1])

        while ret and choice != "exit":
            dlg1 = UserChoice(values=value_list, defval=default)
            ret = BlissDialog([[dlg1]], title="Tomo Setup").show()

            # returns False on cancel
            if ret != False:
                choice = ret[dlg1]
                if choice == "description":
                    self._set_description()
                if choice == "attenuators":
                    self._set_attenuators()
                if choice == "slits":
                    self._set_slits()
                if choice == "motors":
                    self._set_motors()
                if choice == "sequence":
                    self._set_tomo()
                if choice == "detector":
                    self._set_detector()
                self.snapshot.update()

    def _set_description(self):
        ct_list = []
        dlg_list = []
        dlg_list.append(UserInput(label="Name", defval=self.snapshot.name))
        dlg_list.append(UserInput(label="Proposal", defval=self.snapshot.proposal))
        dlg_list.append(UserInput(label="Comment", defval=self.snapshot.comment))
        ct_list.append([Container(dlg_list, title=f"Description")])
        ret = BlissDialog(ct_list, title="Multires").show()
        if ret is not False:
            snapshot_dict = self.snapshot.to_dict()
            name = ret[dlg_list[0]]
            snapshot_dict["name"] = name
            snapshot_dict["proposal"] = ret[dlg_list[1]]
            snapshot_dict["comment"] = ret[dlg_list[2]]
            self._database.update(snapshot_dict, self.query.name == self.snapshot.name)


#    def _set_attenuators_labels(self):
#        dic = self.snapshot.attenuators
#        dlg_list = []
#        for key in dic:
#            if key != "all":
#                obj = current_session.config.get(key)
#                list_labels = [i["label"] for i in obj.positions_list]
#                for x in range(len(list_labels)):
#                    if obj.position == list_labels[x]:
#                        default = x
#                dlg_list.append(UserChoice(label=f"{key}", values=[(x,x) for x in list_labels], defval=default))
#
#        ct1 = Container(
#            dlg_list, title="Attenuators"
#        )
#        ret = BlissDialog([[ct1]], title="Attenuators setup").show()
#        if ret is not False:
#            c = 0
#            for key in dic:
#                if key != "all":
#                    dic[key] = ret[dlg_list[c]]
#                    c += 1
#
#        snapshot_dict = self.snapshot.to_dict()
#        snapshot_dict["attenuators"] = {"all": dic}
#        self._database.update(snapshot_dict, self.query.name == self.snapshot.name)


    def _set_attenuators(self):
        dic = self.snapshot.attenuators['all']
        dlg_list = []
        for key in dic:
            if "_" not in key:
                dlg_list.append(UserFloatInput(label=f"{key} dial [{dic[key+'_label']}]", defval=dic[key + '_dial']))
        ct1 = Container(
            dlg_list, title="Attenuators"
        )
        ret = BlissDialog([[ct1]], title="Multires").show()
        if ret is not False:
            c = 0
            for key in dic:
                if "dial" in key:
                    dic[key] = ret[dlg_list[c]]
                    c += 1

        snapshot_dict = self.snapshot.to_dict()
        snapshot_dict["attenuators"] = {"all": dic}
        self._database.update(snapshot_dict, self.query.name == self.snapshot.name)

    def _set_slits(self):
        dic = self.snapshot.slits
        ct_list = []
        dlg_list = []
        index = 0
        for key in dic:
            start = index
            for slits in dic[key]:
                dlg_list.append(UserFloatInput(label=f"{slits}", defval=dic[key][slits]))
                index += 1
            end = index
            ct_list.append([Container(dlg_list[start:end], title=f"{key}")])
        ret = BlissDialog(ct_list, title="Multires").show()
        if ret is not False:
            c = 0
            for key in dic:
                for slits in dic[key]:
                    dic[key][slits] = ret[dlg_list[c]]
                    c += 1

        snapshot_dict = self.snapshot.to_dict()
        snapshot_dict["slits"] = dic
        self._database.update(snapshot_dict, self.query.name == self.snapshot.name)

    def _set_motors(self):
        dic = self.snapshot.motors
        ct_list = []
        dlg_list = []
        index = 0
        for key in dic:
            start = index
            for motors in dic[key]:
                for pars in dic[key][motors]:
                    dlg_list.append(UserFloatInput(label=f"{motors} {pars}", defval=dic[key][motors][pars]))
                    index += 1
            end = index
            ct_list.append([Container(dlg_list[start:end], title=f"{key}")])
        ret = BlissDialog(ct_list, title="Multires").show()
        if ret is not False:
            c = 0
            for key in dic:
                for motors in dic[key]:
                    for pars in dic[key][motors]:
                        dic[key][motors][pars] = ret[dlg_list[c]]
                        c += 1

        snapshot_dict = self.snapshot.to_dict()
        snapshot_dict["motors"] = dic
        self._database.update(snapshot_dict, self.query.name == self.snapshot.name)

    def _set_tomo(self):
        dic = self.snapshot.tomo
        ct_list = []
        dlg_list = []
        index = 0
        for key in ["config_pars", "basic_pars", "sequence_pars"]:
            if len(dic[key]) == 0:
                continue
            start = index
            for pars in dic[key]:
                if isinstance(dic[key][pars], int) and not isinstance(dic[key][pars], bool):
                    dlg_list.append(UserIntInput(label=f"{pars}", defval=dic[key][pars]))
                elif isinstance(dic[key][pars], float):
                    dlg_list.append(UserFloatInput(label=f"{pars}", defval=dic[key][pars]))
                else:
                    dlg_list.append(UserInput(label=f"{pars}", defval=dic[key][pars]))
                index += 1
            end = index
            ct_list.append([Container(dlg_list[start:end], title=f"{key}")])
        ret = BlissDialog(ct_list, title="Multires").show()
        if ret is not False:
            c = 0
            for key in ["config_pars", "basic_pars", "sequence_pars"]:
                for pars in dic[key]:
                    dic[key][pars] = ret[dlg_list[c]]
                    c += 1

        snapshot_dict = self.snapshot.to_dict()
        snapshot_dict["tomo"] = dic
        self._database.update(snapshot_dict, self.query.name == self.snapshot.name)

    def _set_detector(self):
        dic = self.snapshot.detector["optics"]
        ct_list = []
        dlg_list = []
        index = 0
        for key in dic:
            start = index
            for value in dic[key]:
                if isinstance(dic[key][value], int) and not isinstance(dic[key][value], bool):
                    dlg_list.append(UserIntInput(label=f"{value}", defval=dic[key][value]))
                elif isinstance(dic[key][value], float):
                    dlg_list.append(UserFloatInput(label=f"{value}", defval=dic[key][value]))
                else:
                    dlg_list.append(UserInput(label=f"{value}", defval=dic[key][value]))
                index += 1
            end = index
            ct_list.append([Container(dlg_list[start:end], title=f"{key}")])

        dic_d = self.snapshot.detector["detector"]
        dlg_list_d = []
        index = 0
        for key in dic_d:
            dlg_list_d.append(UserInput(label=f"{key}", defval=dic_d[key]))
        ct_list.append([Container(dlg_list_d, title=f"Detector")])

        ret = BlissDialog(ct_list, title="Multires").show()
        if ret is not False:
            c = 0
            for key in dic:
                for motors in dic[key]:
                    dic[key][motors] = ret[dlg_list[c]]
                    c += 1
            c = 0
            for key in dic_d:
                dic_d[key] = ret[dlg_list_d[c]]
                c += 1

        snapshot_dict = self.snapshot.to_dict()
        snapshot_dict["detector"]["optics"] = dic
        snapshot_dict["detector"]["detector"] = dic_d
        self._database.update(snapshot_dict, self.query.name == self.snapshot.name)

    def show(self, name=None):
        # show the values of a snapshot

        if name is None:
            name = self._menu_snap_name()
            if name is None:
                return

        self._names = [snap["name"] for snap in self._database.all()]
        if name in self._names:
            self.snapshot.switch(name)
            self.searchandshow(name=name)
        else:
            print("Unknown snapshot name!")

    def copy(self, to_name, from_name=None):
        if from_name is None:
            from_name = self._menu_snap_name()
            if from_name is None:
                return

        name = self.snapshot.name
        self.snapshot.switch(from_name)
        snapshot_dict = self.snapshot.to_dict()
        snapshot_dict["name"] = to_name
        self.save(to_name)
        self.snapshot.switch(to_name)
        self.snapshot.from_dict(snapshot_dict)
        if name != None:
            self.snapshot.switch(name)

    def __info__(self):
        """
        Show the snapshots from the last 30 days by default, then the snapshot that was last selected
        """
        s_list = [snap["name"] for snap in self._database.all()]
        info_list = [['name', 'creation_date', 'comment', 'proposal', "pixel_size", "energy", "sample_to_detector_distance"]]

        if len(self._names) == len(s_list):
            for x in self.searchandshow(days=30, show=False):
                info_list.append(x)
            return tabulate(info_list, headers=("firstrow"))
        else:
            for name in self._names:
                info = self.searchandshow(name=name, show=False)
                if len(info) > 0:
                    info_list.append(info[0])
            return tabulate(info_list, headers=("firstrow"))

    def show_all(self):
        self.searchandshow(name="")

    def searchandshow(self, days=None, name=None, proposal=None, pixel_size=None, energy=None, show=True):
        """
        Search snpashots in the database according to parameters
        """
        #
        # To search by pixel_size or energy, use symbols (==,<,>) and the value you are looking for
        # pixel_size = ">2"
        # If you want a pixel_size or energy between two values, you can do
        # energy = [">= 1","< 5"]
        #
        res_str = ""
        Snap = Query()
        request = ""
        if pixel_size is not None:
            if isinstance(pixel_size, list):
                for pz in pixel_size:
                    if request == "":
                        request = f"(Snap.detector.detector.sample_pz {pz})"
                    else:
                        request += f" & (Snap.detector.detector.sample_pz {pz})"
            else:
                if request == "":
                    request = f"(Snap.detector.detector.sample_pz {pixel_size})"
                else:
                    request += f" & (Snap.detector.detector.sample_pz {pixel_size})"

        if energy is not None:
            if isinstance(energy, list):
                for engy in energy:
                    if request == "":
                        request = f"(Snap.tomo.config_pars.energy {engy})"
                    else:
                        request += f" & (Snap.tomo.config_pars.energy {engy})"
            else:
                if request == "":
                    request = f"(Snap.tomo.config_pars.energy {energy})"
                else:
                    request += f" & (Snap.tomo.config_pars.energy {energy})"

        if days is not None:
            if request == "":
                request = f"(Snap.creation_date.test(test_date, {days}))"
            else:
                request += f" & (Snap.creation_date.test(test_date, {days}))"

        if name is not None:
            if request == "":
                request = f"(Snap.name.matches('.*{name}.*'))"
            else:
                request += f" & (Snap.name.matches('.*{name}.*'))"
        if proposal is not None:
            if request == "":
                request = f"(Snap.proposal.matches('.*{proposal}.*'))"
            else:
                request += f" & (Snap.proposal.matches('.*{proposal}.*'))"

        res = self._database.search(eval(request))
        names = []
        res_list = []
        for i in res:
            if "sample_to_detector_distance" in i["tomo"]:
                res_list.append([i['name'], i['creation_date'], i['comment'],i['proposal'], i['detector']['detector']['sample_pz'], i['tomo']['config_pars']['energy'], round(i['tomo']["sample_to_detector_distance"], 2)])
            else:
                # Can remove this code if previous snapshots with missing sample_detector distance are saved again
                res_list.append([i['name'], i['creation_date'], i['comment'],i['proposal'], i['detector']['detector']['sample_pz'], i['tomo']['config_pars']['energy'], "None"])
            names.append(i['name'])
        names.sort()
        if show:
            res_list.insert(0,['name', 'creation_date', 'comment','proposal', "pixel_size", "energy", "sample_to_detector_distance"])
            print(tabulate(res_list, headers=("firstrow")))
            self._names = names
        else:
            if len(res_list) > 0:
                return res_list
            else:
                return []

    def slits(self):
        for key in self.snapshot.slits:
            print(f"{key}:")
            for name, value in self.snapshot.slits[key].items():
                print(f"\t{name}: {value}")

    def attenuators(self):
        for key in self.snapshot.attenuators:
            print(f"{key}:")
            for name, value in self.snapshot.attenuators[key].items():
                print(f"\t{name}: {value}")

    def motors(self):
        for key in self.snapshot.motors:
            print(f"{key}:")
            for name, value in self.snapshot.motors[key].items():
                print(f"\t{name}: {value}")

    def detector(self):
        for key in self.snapshot.detector:
            print(f"{key}:")
            for name, value in self.snapshot.detector[key].items():
                print(f"\t{name}: {value}")

    def tomo(self):
        for key in self.snapshot.tomo:
            print(f"{key}:")
            for name, value in self.snapshot.tomo[key].items():
                print(f"\t{name}: {value}")

# Class to handle slit positions in a snapshot
class slitsSnap:
    def __init__(self, name, config):

        # get all slits to snapshot
        self.slits_list = config.get("slits")

    def _init_default(self):
        # init slit default or reference valvues

        return {}

    def __info__(self):
        """
        Show the slits from the current snapshot
        """
        return ""

    def save(self):
        # Read positions from all specified slits

        slits_dict = {}
        for s in self.slits_list:
            s_config = self.slits_list[s]

            m_dict = {}
            for m in s_config['motors']:
                m_dict[m.name] = m.position
                m_dict[m.name + "_dial"] = m.dial
            slits_dict[s] = m_dict
        return slits_dict

    def apply(self, slits_dict):
        # Move all slits to the stored positions
        args = []
        for s in slits_dict:
            print("Loading positions to %s" % s)
            for mot in self.slits_list[s]['motors']:
                args.append(mot)
                user_pos = mot.sign * slits_dict[s][mot.name + "_dial"] + mot.offset
                args.append(user_pos)
        return args



class detectorSnap:
    def __init__(self, name, config):
        # get all optics to snapshot
        self.config = current_session.config
        self.det_dict = {}
        if tomo is not None:
            tomo_config = config.get('tomo')
            sequence = tomo_config.get("sequence")
            self.opiom = tomo_config.get("opiom")
            self.detector = sequence._detector
            det_optics = tomo_config.get("detectors_optic")
            for det in det_optics.detectors:
                self.det_dict[det_optics.get_detector_name(det)] = det
        else:
            self.opiom = None
            self.detector = None

    def _init_default(self):
        # init motor default or reference valvues
        return {}

    def save(self):
        # Read positions from all specified optics
        active_mg = get_active_mg()
        enabled = active_mg.enabled if active_mg else False

        active_det = None
        self.optics = []
        for i in enabled:
            if ":image" in i:
                detector = self.det_dict[i.split(":")[0]]
                self.optics.append(detector.optic)
                active_det = {}
                active_det['name'] = detector.name
                detector.sync_hard()
                active_det['sample_pz'] = detector.sample_pixel_size
                active_det['roi'] = detector.detector.image.roi
                active_det['binning'] = detector.detector.image.binning
                active_det['flip'] = detector.detector.image.flip
                active_det['rotation'] = detector.detector.image.rotation
                # active_det['depth'] = detector.detector.image.depth
                active_det['accumulation'] = detector.detector.acquisition.mode
                active_det['max_expo_time'] = detector.detector.accumulation.max_expo_time
        optics_dict = {}
        count_dict = {}
        count_dict['enabled'] = enabled
        for optic in self.optics:
            optic_dict = {}
            for motor in optic.list_motors():
                optic_dict[motor.name] = motor.position
                optic_dict[motor.name + "_dial"] = motor.dial
            if type(optic) == TwinOptic:
                optic_dict["objective"] = optic.objective
            optic_dict["magnification"] = optic.magnification
            optics_dict[optic.name] = optic_dict

        if active_det is None:
            return self._init_default()
        detector_dict = {"optics":optics_dict, "counters":count_dict, "detector":active_det}
        if self.opiom:
            opiom_output = self.opiom.getGlobalStat()
            detector_dict.update({"opiom": opiom_output})

        return detector_dict

    def clean(self, det_dict):
        det_dict["detector"]["sample_pz"] = float(det_dict["detector"]["sample_pz"])
        det_dict["detector"]["rotation"] = str(det_dict["detector"]["rotation"])
        # det_dict["detector"]["depth"] = float(det_dict["detector"]["depth"])
        det_dict["detector"]["roi"] = ast.literal_eval(det_dict["detector"]["roi"])
        det_dict["detector"]["binning"] = ast.literal_eval(det_dict["detector"]["binning"])
        det_dict["detector"]["flip"] = ast.literal_eval(det_dict["detector"]["flip"])
        det_dict["detector"]["max_expo_time"] = float(det_dict["detector"]["max_expo_time"])
        return det_dict

    def apply(self, detector_dict, optic_group=None):
        count_dict = detector_dict["counters"]
        optics_dict = detector_dict["optics"]
        if "opiom" in detector_dict:
            for key, value in detector_dict["opiom"].items():
                self.opiom.switch(key, value)
        active_det_dict = detector_dict["detector"]
        ACTIVE_MG.disable_all()
        ACTIVE_MG.enable(*count_dict['enabled'])
        # Move all motors to the stored positions
        detector = self.config.get(active_det_dict["name"])
        if "accumulation" in active_det_dict:
            detector.detector.acquisition.mode = active_det_dict['accumulation']
            if active_det_dict['accumulation'] == "ACCUMULATION":
                set_int_accumulation(detector.detector)
            else:
                set_int_trigger(detector.detector)
        detector.detector.accumulation.max_expo_time = active_det_dict['max_expo_time']
        detector.acq_mode =  detector.detector.acquisition.mode

        #if "depth" in active_det_dict:
        #    detector.detector.image.depth = float(active_det_dict['depth'])
        if "binning" in active_det_dict:
            if not isinstance(active_det_dict['binning'], list):
                active_det_dict['binning'] = ast.literal_eval(active_det_dict['binning'])
            detector.detector.image.binning = active_det_dict['binning']
        if "flip" in active_det_dict:
            if not isinstance(active_det_dict['flip'], list):
                active_det_dict['flip'] = ast.literal_eval(active_det_dict['flip'])
            detector.detector.image.flip = active_det_dict['flip']
        if "rotation" in active_det_dict:
            detector.detector.image.rotation = str(active_det_dict['rotation'])
        if not isinstance(active_det_dict['roi'], list):
            active_det_dict['roi'] = ast.literal_eval(active_det_dict['roi'])
        detector.detector.image.roi = active_det_dict['roi']
        detector.user_unbinned_sample_pixel_size = float(active_det_dict['sample_pz'])/detector.detector.image.binning[0]


        args = []
        for optic in optics_dict:
            optic = self.config.get(optic)
            if type(optic) == TwinOptic:
                objective, = [optic.available_magnifications.index(m)+1 for m in optic.available_magnifications if m == optics_dict[optic.name]["magnification"]]
                optic.objective = objective
            else:
                optic.magnification  = optics_dict[optic.name]["magnification"]
            for motor in optic.list_motors():
                args.append(motor)
                args.append(optics_dict[optic.name][motor.name])

        return args

class motorsSnap:
    def __init__(self, name, config):

        # get all motors to snapshot
        self.motors_list = config.get('motors')

    def _init_default(self):
        # init motor default or reference valvues

        return {}

    def save(self):
        # Read positions from all specified motors

        motors_dict = {}
        for mot in self.motors_list:
            mot_config = self.motors_list[mot]

            m_dict = {}
            for m in mot_config['motors']:
                if isinstance(m,MultiplePositions):
                    m_dict[m.name] = {"position": m.position}
                else:
                    m_dict[m.name] = {"position": m.position, "dial": m.dial}

            motors_dict[mot] = m_dict

        if mot_config.get('detectors_stage') is not None:
            det_stage = mot_config['detectors_stage']
            lmotors = [target['axis'].name for pos in det_stage.positions_list for target in pos['target'] if pos['label'] == det_stage.position]
            detectors_stage_dict = {"motors": lmotors}
            motors_dict.update({"detectors_stage": detectors_stage_dict})

        return motors_dict

    def apply(self, motors_dict, mot_group=None):
        # Move all motors to the stored positions
        if mot_group == None:
            for mot_group in motors_dict:
                print("Applying positions to %s" % mot_group)
                args = []
                for mot in self.motors_list[mot_group]['motors']:
                    if mot.name not in motors_dict[mot_group]:
                        print(f"{mot.name} is missing")
                        continue
                    if "yrot" in mot.name:
                        user_pos = mot.sign * motors_dict[mot_group][mot.name]["dial"] + mot.offset
                        args.append(mot)
                        args.append(user_pos)
                    elif isinstance(mot,MultiplePositions) and mot.position != motors_dict[mot_group][mot.name]["position"]:
                        mot_pos = [(d["axis"],d["destination"]) for label,dictionnary in mot.targets_dict.items() if label == motors_dict[mot_group][mot.name]["position"] for d in mot.targets_dict[label]]
                        args.extend(list(sum(mot_pos,())))
                    else:
                        if self.motors_list[mot_group].get("detectors_stage") is not None and mot.name in motors_dict["detectors_stage"]["motors"]:
                            args.append(mot)
                            args.append(motors_dict[mot_group][mot.name]["position"])

        else:
            print("Applying positions to %s" % mot_group)
            args = []
            for mot in self.motors_list[mot_group]['motors']:
                if mot.name not in motors_dict[mot_group]:
                        print(f"{mot.name} is not in {mot_group}")
                        continue
                if "yrot" in mot.name:
                    user_pos = mot.sign * motors_dict[mot_group][mot.name]["dial"] + mot.offset
                    args.append(mot)
                    args.append(user_pos)
                elif isinstance(mot,MultiplePositions) and mot.position != motors_dict[mot_group][mot.name]["position"]:
                    mot_pos = [(d["axis"],d["destination"]) for label,dictionnary in mot.targets_dict.items() if label == motors_dict[mot_group][mot.name]["position"] for d in mot.targets_dict[label]]
                    args.extend(list(sum(mot_pos,())))
                else:
                    #if motors_dict.get("detectors_stage") is not None:
                    #    if self.motors_list[mot_group].get("detectors_stage") is not None and mot.name in motors_dict["detectors_stage"]["motors"]:
                    #        args.append(mot)
                    #        args.append(motors_dict[mot_group][mot.name]["position"])
                    #else:
                    args.append(mot)
                    args.append(motors_dict[mot_group][mot.name]["position"])
        return args


class attenuatorsSnap:
    def __init__(self, name, config):

        # get all attenuators to snapshot
        self.attenuators_list = config.get('attenuators', [])

    def _init_default(self):
        # init attenuator default or reference values
        # breakpoint()
        return {}

    def save(self):
        # Read positions from all specified attenuators

        attenuators_dict = {}
        for att in self.attenuators_list:
            att_config = self.attenuators_list[att]

            a_dict = {}
            for a in att_config['attenuators']:
                a_dict[a.name] = a.position
                for m in a.motor_objs:
                    a_dict[m.name] = m.position
            attenuators_dict[att]=a_dict
        return attenuators_dict

    def apply(self, attenuators_dict, att_group=None):
        # Move all attenuators to the stored positions
        args = []
        if att_group == None:
            for att_group in attenuators_dict:
                print("Applying positions to %s" % att_group)
                for att in self.attenuators_list[att_group]['attenuators']:
                    if att.position != attenuators_dict[att_group][att.name]:
                        for mot in att.motor_objs:
                            pos_dict = [dictionnary for dictionnary in att.targets_dict[attenuators_dict[att_group][att.name]] if dictionnary['axis'] == mot]
                            pos_dict = pos_dict[0]
                            if mot.name in attenuators_dict[att_group]:
                                args.append(mot)
                                args.append(attenuators_dict[att_group][mot.name])
                            else:
                                args.append(mot)
                                args.append(pos_dict.get("destination"))
        else:
            print("Applying positions to %s" % att_group)
            for att in self.attenuators_list[att_group]['attenuators']:
                if att.position != attenuators_dict[att_group][att.name]:
                    for mot in att.motor_objs:
                        pos_dict = [dictionnary for dictionnary in att.targets_dict[attenuators_dict[att_group][att.name]] if dictionnary['axis'] == mot]
                        pos_dict = pos_dict[0]
                        if mot.name in attenuators_dict[att_group]:
                            args.append(mot)
                            args.append(attenuators_dict[att_group][mot.name])
                        else:
                            args.append(mot)
                            args.append(pos_dict.get("destination"))
        return args


class tomoSnap:
    def __init__(self, name, config):

        # get tomo to snapshot
        self.tomo_config = config.get('tomo')
        self.sequence = self.tomo_config.get("sequence")
        self.config = current_session.config

    def _init_default(self):
        # init tomo default or reference values
        tomo_dict = {}
        tomo_dict['basic_pars'] = {}
        tomo_dict['config_pars'] = {}
        tomo_dict['sequence_pars'] = {}
        tomo_dict['reference'] = {}
        tomo_dict['sample_to_detector_distance'] = ""

        return tomo_dict

    def save(self):
        # Read tomo configuration
        tomo_dict = {}
        tomo_dict['reference'] = {}
        for pars in ["basic_pars", "config_pars", "sequence_pars"]:
            tomo_dict[pars] = self.sequence.__getattribute__(pars).to_dict()

        tomo_dict["basic_pars"].pop("save_flag")
        tomo_dict["basic_pars"].pop("display_flag")
        tomo_dict["basic_pars"].pop("move_to_acquisition_position")
        tomo_dict["basic_pars"].pop("shift_in_fov_factor")
        tomo_dict["basic_pars"].pop("shift_in_pixels")
        tomo_dict['reference']["motors"] = [motor.name for motor in self.sequence._reference.ref_motors]
        tomo_dict['reference']["in"] = self.sequence._reference.parameters.in_beam_position
        tomo_dict['reference']["out"] = self.sequence._reference.parameters.out_of_beam_position
        tomo_dict['reference']["disp"] = self.sequence._reference.parameters.out_of_beam_displacement
        tomo_dict['reference']["absolute_move"] = self.sequence._reference.parameters.absolute_move
        tomo_dict['sample_to_detector_distance'] = self.sequence._detectors.get_tomo_detector(self.sequence._detector).sample_detector_distance
        return tomo_dict

    def apply(self, tomo_dict):
        # Apply tomo configuration
        pars_dict = {**tomo_dict["basic_pars"], **tomo_dict["config_pars"], **tomo_dict["sequence_pars"]}
        self.sequence.pars.from_dict(pars_dict)
        self.sequence._tomo_config.energy = self.sequence.pars.energy
        self.sequence._reference.ref_motors = [self.config.get(motor) for motor in tomo_dict['reference']["motors"]]
        self.sequence._reference.parameters.in_beam_position = tomo_dict['reference']["in"]
        self.sequence._reference.parameters.out_of_beam_position = tomo_dict['reference']["out"]
        self.sequence._reference.parameters.out_of_beam_displacement = tomo_dict['reference']["disp"]
        self.sequence._reference.parameters.absolute_move = tomo_dict['reference']["absolute_move"]

    def clean(self, tomo_dict):
        for pars in ["basic_pars", "config_pars", "sequence_pars"]:
            seq_dict = self.sequence.__getattribute__(pars).to_dict()
            for par in seq_dict:
                if par in tomo_dict[pars]:
                    # print(par)
                    if isinstance(seq_dict[par], float):
                        tomo_dict[pars][par] = float(tomo_dict[pars][par])
                    elif isinstance(seq_dict[par], int) and not isinstance(seq_dict[par], bool):
                        tomo_dict[pars][par] = int(float(tomo_dict[pars][par]))
                    else:
                        tomo_dict[pars][par] = str(tomo_dict[pars][par])
        return tomo_dict
