from bliss.scanning.chain import ChainPreset
from bliss.config.static import get_config


class ID06SanityPreset(ChainPreset):
    def prepare(self, chain):
        config = get_config()
        pico1 = config.get("pico1")
        icxpos = config.get("icxpos")

        if list(chain.get_node_from_devices(pico1))[0] is not None:
            if icxpos.position != "pico1":
                raise RuntimeError("pico1 is used but is not mounted. Use `pico_in() to move it on the beam`")
